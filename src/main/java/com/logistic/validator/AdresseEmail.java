package com.logistic.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import java.lang.annotation.*;

/**
 * Annotation de validation d'une adresse email
 *
 * @author LePro
 */

@NotNull(message = "L'adresse mail est obligatoire")
@Pattern(
        regexp = "^([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5})$",
        message = "Le format de l'adresse mail est invalide")
@Target(value = {ElementType.TYPE_USE, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {})
@Documented
public @interface AdresseEmail {
    String message() default "Le format de l'adresse mail est invalide";
    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
