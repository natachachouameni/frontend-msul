package com.logistic.mapper;

import com.logistic.entities.ClientEntity;
import com.logistic.entities.DeliveryEntity;
import com.logistic.entities.UserEntity;
import com.logistic.entities.model.Client;
import com.logistic.entities.model.Delivery;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface ClientMapper {
  public static final ClientMapper INSTANCE = Mappers.getMapper(ClientMapper.class);

  Client convertEntityClientToClient(ClientEntity deliveryEntity);

  ClientEntity convertClientToClientEntity(Client delivery);

  UserEntity convertClientToUserEntity(Client delivery);
}
