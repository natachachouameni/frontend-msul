package com.logistic.mapper;

import com.logistic.entities.DeliveryEntity;
import com.logistic.entities.model.Delivery;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface DeliveryMapper {
  public static final DeliveryMapper INSTANCE = Mappers.getMapper(DeliveryMapper.class);

  Delivery  convertEntityDeliveryToDelivery(DeliveryEntity deliveryEntity);

  DeliveryEntity  convertDeliveryToDeliveryEntity(Delivery delivery);
}
