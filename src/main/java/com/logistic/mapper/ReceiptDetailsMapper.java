package com.logistic.mapper;

import com.logistic.entities.ClientEntity;
import com.logistic.entities.DeliveryDetailsEntity;
import com.logistic.entities.model.Client;
import com.logistic.entities.model.DeliveryDetails;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
@Mapper(componentModel = "spring")
public interface ReceiptDetailsMapper  {
    public static final ReceiptDetailsMapper INSTANCE = Mappers.getMapper(ReceiptDetailsMapper.class);

   DeliveryDetails convertEntityDeliveryDetailsToDeliveryDetails(DeliveryDetailsEntity deliveryDetailsEntity);

    DeliveryDetailsEntity convertDeliveryDetailsToDeliveryDetailsEntity(DeliveryDetails deliveryDetails);
}
