package com.logistic.mapper;

import com.logistic.entities.DeliveryCaptureEntity;

import com.logistic.entities.UoMEntity;
import com.logistic.entities.model.DeliveryCapture;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface DeliveryCaptureMapper {
    public static final DeliveryCaptureMapper INSTANCE = Mappers.getMapper(DeliveryCaptureMapper.class);

    @Mapping(source = "stock.unit", target = "stock.unit", qualifiedByName = "convertUnitStr2UnitEntity")
    DeliveryCapture convertEntityDeliveryCaptureToDeliveryCapture(DeliveryCaptureEntity deliveryCaptureEntity);

    @Mapping(source = "stock.unit", target = "stock.unit", ignore = true)
    DeliveryCapture convertDeliveryCaptureToDeliveryCaptureEntity(DeliveryCapture deliveryCapture);


    @Named("convertUnitStr2UnitEntity")
    default String convertUnitStr2UnitEntity(UoMEntity uoMEntity) {
        return null != uoMEntity ? uoMEntity.getUnit() : "";
    }
}