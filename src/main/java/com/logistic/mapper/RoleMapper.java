package com.logistic.mapper;

import com.logistic.entities.ClientEntity;
import com.logistic.entities.RoleEntity;
import com.logistic.entities.model.Client;
import com.logistic.entities.model.Role;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface RoleMapper {
  public static final RoleMapper INSTANCE = Mappers.getMapper(RoleMapper.class);

  Role convertRoleEntityToRole(RoleEntity roleEntity);

  RoleEntity convertRoleToRoleEntity(Role role);
}
