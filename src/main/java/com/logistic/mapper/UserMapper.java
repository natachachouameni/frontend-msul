package com.logistic.mapper;

import com.logistic.entities.RoleEntity;
import com.logistic.entities.UserEntity;
import com.logistic.entities.model.User;
import com.logistic.enums.AppRole;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserMapper {
    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    @Named("buildRole")
    static AppRole buildRole(List<RoleEntity> roles) {
        String role = roles.stream().findAny().map(roleEntity -> roleEntity.getName()).orElse(null);
        return null == role ? null : AppRole.valueOf(role);
    }

    @Mapping(source = "roles", target = "role", qualifiedByName = "buildRole")
    User convertUserEntityToUser(UserEntity userEntity);

    UserEntity convertUserToUserEntity(User userDto);
}
