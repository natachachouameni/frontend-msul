package com.logistic.mapper;

import com.logistic.entities.ClientEntity;
import com.logistic.entities.DeliveryDetailsEntity;
import com.logistic.entities.model.DeliveryDetails;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface DeliveryDetailsMapper {
    public static final DeliveryDetailsMapper INSTANCE = Mappers.getMapper(DeliveryDetailsMapper.class);
    DeliveryDetails convertEntityDeliveryDetailsToDeliveryDetails(DeliveryDetailsEntity deliveryDetailsEntity);

   DeliveryDetails convertDeliveryDetailsToDeliveryDetailsEntity(DeliveryDetails deliveryDetails);
}
