package com.logistic.mapper;

import com.logistic.entities.DeliveryEntity;
import com.logistic.entities.ReceiptEntity;
import com.logistic.entities.model.Delivery;
import com.logistic.entities.model.Receipt;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface  ReceiptMapper {
    public static final ReceiptMapper INSTANCE = Mappers.getMapper(ReceiptMapper.class);

    Receipt convertEntityReceiptToReceipt(ReceiptEntity receiptEntity) ;

    ReceiptEntity  convertReceiptToReceiptEntity(Receipt receipt);
}
