package com.logistic.mapper;

import com.logistic.entities.StockEntity;
import com.logistic.entities.UoMEntity;
import com.logistic.entities.model.Stock;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface StockMapper {
    public static final StockMapper INSTANCE = Mappers.getMapper(StockMapper.class);

    @Mapping(target = "unit", source = "unit", qualifiedByName = "convertUnitStr2UnitEntity")
    Stock convertEntityStockToStock(StockEntity stockEntity);

    @Mapping(target = "unit", source = "unit", ignore = true)
    StockEntity convertStockToStockEntity(Stock stock);

    @Named("convertUnitStr2UnitEntity")
    default String convertUnitStr2UnitEntity(UoMEntity uoMEntity) {
        return null != uoMEntity ? uoMEntity.getUnit() : "";
    }
}
