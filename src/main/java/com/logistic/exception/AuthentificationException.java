package com.logistic.exception;

public class AuthentificationException extends RuntimeException{
    public AuthentificationException(String message) {
        super(message);
    }

    public AuthentificationException(Exception exception) {
        super(exception);
    }

    public AuthentificationException(Exception exception, String message) {
        super(message, exception);
    }
}
