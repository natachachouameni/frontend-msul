package com.logistic.exception;

public class UserException extends RuntimeException {

    public UserException(String message){
        super(message);
    }

    public static class UserAlreadyExistException extends UserException{

        public UserAlreadyExistException(String message) {
            super(message);
        }
    }
}
