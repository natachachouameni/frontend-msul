package com.logistic.exception;

public class ClientException extends RuntimeException {

    public ClientException(String message){
        super(message);
    }
}
