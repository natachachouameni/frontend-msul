package com.logistic.services;

import com.logistic.entities.DeliveryDetailsEntity;
import com.logistic.entities.model.DeliveryDetails;
import com.logistic.exception.MessageException;
import com.logistic.exception.ResourceNotFoundException;
import com.logistic.mapper.DeliveryDetailsMapper;
import com.logistic.repositories.DeliveryDetailsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


    @Service
    @RequiredArgsConstructor
    @Transactional(readOnly = true)
    public class DeliveryDetailsService {
        private final DeliveryDetailsRepository deliveryDetailsRepository;

        public List<DeliveryDetails> findAllDeliveriesDetailsByDeliveryEntityId(Long DeliveryId) {
            return   deliveryDetailsRepository.findAllByDeliveryEntityId(DeliveryId).stream()
                    .map(DeliveryDetailsMapper.INSTANCE::convertEntityDeliveryDetailsToDeliveryDetails)
                    .collect(Collectors.toList());
        }
        public List<DeliveryDetails> findAllDeliveriesDetailsByStockEntityId(BigInteger StockId) {
            return   deliveryDetailsRepository.findAllByStockEntityId(StockId).stream()
                    .map(DeliveryDetailsMapper.INSTANCE::convertEntityDeliveryDetailsToDeliveryDetails)
                    .collect(Collectors.toList());
        }
        public List<DeliveryDetails> findAllDeliveriesByFilter(String recherche) {
            return deliveryDetailsRepository.findByCriteria(recherche).stream()
                    .map(DeliveryDetailsMapper.INSTANCE::convertEntityDeliveryDetailsToDeliveryDetails)
                    .collect(Collectors.toList());

        }
        public List<DeliveryDetails> findAll() {
            return deliveryDetailsRepository.findAll().stream()
                    .map(DeliveryDetailsMapper.INSTANCE::convertEntityDeliveryDetailsToDeliveryDetails)
                    .collect(Collectors.toList());

        }
        public List<DeliveryDetails> findDeliveryDetailsEntitiesById(Long id) {
            return deliveryDetailsRepository.findDeliveryDetailsEntitiesById(id).stream()
                    .map(DeliveryDetailsMapper.INSTANCE::convertEntityDeliveryDetailsToDeliveryDetails)
                    .collect(Collectors.toList());

        }
        @Transactional
        public DeliveryDetails saveDeliveryDetails(DeliveryDetails deliveryDetails) {
            Optional<DeliveryDetailsEntity> optionalDeliveryDetails = deliveryDetailsRepository.findDeliveryDetailsEntitiesById(deliveryDetails.getId());
            DeliveryDetailsEntity deliveryDetailsEntity ;

            if (optionalDeliveryDetails.isPresent()) {
                deliveryDetailsEntity = optionalDeliveryDetails.get();
                deliveryDetailsEntity.setQtyDelivered(deliveryDetails.getQtyDelivered());
                deliveryDetailsEntity.setUnit(deliveryDetails.getUnit());
                deliveryDetailsEntity.setComments(deliveryDetails.getComments());
                deliveryDetailsEntity.setDeliveryEntity(deliveryDetailsEntity.getDeliveryEntity());
               // deliveryDetailsEntity.setStockEntity(deliveryDetails.getStock());
            }else {
                return DeliveryDetailsMapper.INSTANCE.convertEntityDeliveryDetailsToDeliveryDetails(optionalDeliveryDetails
                        .orElseThrow(() -> new ResourceNotFoundException(MessageException.RESOURCE_FOUND)));

            }

            return saveDeliveryDetails(deliveryDetails);
}
    }

