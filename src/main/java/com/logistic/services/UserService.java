package com.logistic.services;


import com.logistic.entities.RoleEntity;
import com.logistic.entities.UserEntity;
import com.logistic.entities.model.User;
import com.logistic.enums.AppRole;
import com.logistic.exception.AuthentificationException;
import com.logistic.exception.MessageException;
import com.logistic.exception.ResourceNotFoundException;
import com.logistic.exception.UserException;
import com.logistic.mapper.UserMapper;
import com.logistic.repositories.RoleRepository;
import com.logistic.repositories.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class UserService {
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;

    public UserEntity findUserByLogin(String login) {
        return userRepository.findByLogin(login).orElseThrow(() -> new AuthentificationException(MessageException.RESOURCE_FOUND));
    }

    @Transactional
    public UserEntity saveUser(User user) {
        Optional<UserEntity> optionalUser = userRepository.findByLogin(user.getLogin());

        if (optionalUser.isPresent()) {
            throw new UserException.UserAlreadyExistException(String.format("Un utilisateur existe déjà avec le login %s", user.getLogin()));
        }
        UserEntity userEntity = UserMapper.INSTANCE.convertUserToUserEntity(user);
        userEntity.setPassword(passwordEncoder.encode(user.getPassword()));

        RoleEntity role = roleRepository.findByName(user.getRole().toString()).orElseThrow(() -> new ResourceNotFoundException(String.format("Aucun role: %s trouvé", user.getRole())));
        userEntity.addRole(role);
        var saveUser = userRepository.save(userEntity);
        role.getUsers().add(saveUser);

        return saveUser;
    }

    public UserEntity findByUser(long id) {
        return userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(MessageException.RESOURCE_FOUND));
    }

    public List<AppRole> getAllRole() {
        return Arrays.stream(AppRole.values()).collect(Collectors.toList());
    }

    public List<UserEntity> findAllUsers() {
        return userRepository.findAll();
    }
}
