package com.logistic.services;

import com.logistic.entities.OrderEntity;
import com.logistic.entities.OrderItemEntity;
import com.logistic.entities.UserEntity;
import com.logistic.entities.model.Order;
import com.logistic.enums.OrderStatus;
import com.logistic.enums.OrderType;
import com.logistic.exception.ResourceNotFoundException;
import com.logistic.repositories.OrderItemRepository;
import com.logistic.repositories.OrderRepository;
import com.logistic.repositories.StockRepository;
import com.logistic.security.IAuthenticationFacade;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Service
@Slf4j
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class OrderService {
    private final IAuthenticationFacade authenticationFacade;
    private final OrderRepository orderRepository;
    private final OrderItemRepository orderItemRepository;
    private final StockRepository stockRepository;

    public List<OrderEntity> findAllByStatusAndType(OrderStatus status, OrderType type) {
        return orderRepository.findAllByStatusAndType(status, type);
    }

    public List<OrderEntity> findAllBySearchCriteria(String search) {
        return orderRepository.findAllByFilter(search);
    }

    public OrderEntity findByReference(String reference) {
        return orderRepository.findByReference(reference)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("La commande [%s] n'existe pas", reference)));
    }

    /**
     * Save new order
     * @param order  order
     */
    @Transactional
    public void saveOrder(Order order){
        UserEntity currentUser = authenticationFacade.currentUser();
        OrderEntity orderEntity;
        if (Objects.nonNull(order.getReference())){
            orderEntity = this.findByReference(order.getReference());
            orderItemRepository.removeAllOrderIdemByOrderReference(order.getReference());
        } else {
         orderEntity = new OrderEntity()
                 //Todo create util class to generate reference
                .setReference("ORD" + UUID.randomUUID().toString().substring(0, 10));
        }

        orderEntity.setType(order.getType())
                .setDescription(order.getDescription())
                .setStatus(OrderStatus.EN_COURS)
                .setClientEntity(currentUser.getClient())
                .setUserCreated(currentUser);

        if (OrderType.DELIVERY == order.getType()) {
            orderEntity.setDeliveryDate(order.getDate());
        } else {
            orderEntity.setReceiptDate(order.getDate());
        }

        OrderEntity savedOrder = orderRepository.save(orderEntity);

        if (!CollectionUtils.isEmpty(order.getStockOrders())) {
            order.getStockOrders().forEach(stockOrder -> {
                OrderItemEntity orderItem = new OrderItemEntity();
                orderItem.setStock(stockOrder.getStock());
                orderItem.setQuantity(stockOrder.getQuantity());
            });
        }

    }

    @Transactional
    public void validOrder(String reference){
        OrderEntity order = findByReference(reference);
        updateOrderStatus(reference, OrderStatus.VALIDER);
    }

    @Transactional
    public void updateOrderStatus(String reference, OrderStatus status){
        OrderEntity order = findByReference(reference);
        order.setStatus(status);
        orderRepository.save(order);
    }
}
