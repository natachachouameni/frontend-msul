package com.logistic.services;

import com.logistic.entities.*;
import com.logistic.entities.model.Delivery;
import com.logistic.enums.OrderStatus;
import com.logistic.mapper.DeliveryMapper;
import com.logistic.repositories.DeliveryDetailsRepository;
import com.logistic.repositories.DeliveryRepository;
import com.logistic.repositories.StockRepository;
import com.logistic.security.IAuthenticationFacade;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class DeliveryService {
    private final IAuthenticationFacade authenticationFacade;
    private final DeliveryRepository deliveryRepository;
    private final DeliveryDetailsRepository deliveryDetailsRepository;
    private final StockRepository stockRepository;
    private final OrderService orderService;

    public List<Delivery> findAllDeliveriesByReferenceClient(String referenceClient) {
        return deliveryRepository.findAllByClientEntityReference(referenceClient).stream().map(DeliveryMapper.INSTANCE::convertEntityDeliveryToDelivery).collect(Collectors.toList());
    }

    public List<Delivery> findAllDeliveriesByFilter(String recherche) {
        return deliveryRepository.findByCriteria(recherche).stream().map(DeliveryMapper.INSTANCE::convertEntityDeliveryToDelivery).collect(Collectors.toList());

    }

    public List<Delivery> findAll() {
        return deliveryRepository.findAll().stream().map(DeliveryMapper.INSTANCE::convertEntityDeliveryToDelivery).collect(Collectors.toList());

    }

    @Transactional
    public void saveDelivery(Delivery delivery, String referenceOrder) {
        UserEntity currentUser = authenticationFacade.currentUser();
        OrderEntity order = orderService.findByReference(referenceOrder);

        DeliveryEntity deliveryEntity = DeliveryMapper.INSTANCE.convertDeliveryToDeliveryEntity(delivery);
        deliveryEntity.setClientEntity(order.getClientEntity());
        deliveryEntity.setUserCreated(currentUser);

        DeliveryEntity savedDelivery = deliveryRepository.save(deliveryEntity);

        // save delivery detail
        order.getItemEntities().forEach(orderItemEntity -> {
            DeliveryDetailsEntity deliveryDetails = new DeliveryDetailsEntity();
            StockEntity stockEntity = orderItemEntity.getStock();

            deliveryDetails.setDeliveryEntity(savedDelivery)
                    .setUnit(null != stockEntity.getUnit() ? stockEntity.getUnit().getUnit() : null)
                    .setStockEntity(stockEntity)
                    .setQtyDelivered(orderItemEntity.getQuantity())
                    .setUserCreated(currentUser);

            deliveryDetailsRepository.save(deliveryDetails);

        });

        // update stock quantity
        Map<StockEntity, Integer> deliveriesOrders = order.getItemEntities().stream()
                .collect(Collectors.groupingBy(OrderItemEntity::getStock, Collectors.summingInt(OrderItemEntity::getQuantity)));
        deliveriesOrders.forEach((stockEntity, deliveryQt) -> {
            stockEntity.setBalanceStockQty(stockEntity.getBalanceStockQty() - deliveryQt);
            stockRepository.save(stockEntity);
        });

        // update order status
        orderService.updateOrderStatus(order.getReference(), OrderStatus.DELIVERED);

    }


}
