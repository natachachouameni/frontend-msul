package com.logistic.services;

import com.logistic.entities.ClientEntity;
import com.logistic.entities.UserEntity;
import com.logistic.entities.model.Client;
import com.logistic.entities.model.User;
import com.logistic.enums.AppRole;
import com.logistic.mapper.ClientMapper;
import com.logistic.repositories.ClientRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class ClientService {
    private final ClientRepository clientRepository;
    private final UserService userService;

    /**
     * Find all client
     *
     * @return list client
     */
    public List<Client> findAllClient() {
        return clientRepository.findAll().stream()
                .map(ClientMapper.INSTANCE::convertEntityClientToClient)
                .collect(Collectors.toList());
    }

    /**
     * Find client by reference
     *
     * @param reference reference client
     * @return client
     */
    public Client findByClientReference(String reference) {
        Optional<ClientEntity> optionalClient = clientRepository.findByReference(reference);
        return ClientMapper.INSTANCE.convertEntityClientToClient(optionalClient
                .orElseThrow(() -> new RuntimeException(String.format("Client %s n'existe pas", reference))));
    }

    /**
     * Find client by criteria
     *
     * @param recherche criteria
     * @return list client
     */
    public List<Client> findAllClientByFilter(String recherche) {
        return clientRepository.findByCriteria(recherche).stream()
                .map(ClientMapper.INSTANCE::convertEntityClientToClient)
                .collect(Collectors.toList());
    }

    /**
     * Creation d'un nouveau client
     * @param client
     */
    @Transactional
    public ClientEntity saveClient(Client client) {

        ClientEntity clientEntity = ClientMapper.INSTANCE.convertClientToClientEntity(client);
        User userDto = new User();
        userDto.setFirstname(client.getFirstname())
                .setLogin(client.getLogin())
                .setLastname(client.getLastname())
                .setPassword(client.getPassword())
                .setRole(AppRole.ROLE_USER);
        UserEntity user = userService.saveUser(userDto);

        clientEntity.setUser(user);
       return clientRepository.save(clientEntity);

    }
}
