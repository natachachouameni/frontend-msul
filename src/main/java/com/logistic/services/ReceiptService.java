package com.logistic.services;

import com.logistic.entities.*;
import com.logistic.entities.model.Receipt;
import com.logistic.enums.OrderStatus;
import com.logistic.mapper.ReceiptMapper;
import com.logistic.repositories.*;
import com.logistic.security.IAuthenticationFacade;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.*;
import java.util.stream.Collectors;
import java.util.List;


    @Service
    @RequiredArgsConstructor
    @Transactional(readOnly = true)
    public class ReceiptService {
        private final ReceiptRepository receiptRepository;
        private final IAuthenticationFacade authenticationFacade;
        private final ReceiptDetailsRepository receiptDetailsRepository;
        private final StockRepository stockRepository;
        private final OrderService orderService;


        public List<Receipt> findAllReceiptByReferenceClient(String referenceClient) {
            return   receiptRepository.findAllByClientEntityReference(referenceClient).stream()
                    .map(ReceiptMapper.INSTANCE::convertEntityReceiptToReceipt)
                    .collect(Collectors.toList());
        }
        public List<Receipt> findAllReceiptByFilter(String recherche) {
            return receiptRepository.findByCriteria(recherche).stream()
                    .map(ReceiptMapper.INSTANCE::convertEntityReceiptToReceipt)
                    .collect(Collectors.toList());

        }
        public List<Receipt> findAll() {
            return receiptRepository.findAll().stream()
                    .map(ReceiptMapper.INSTANCE::convertEntityReceiptToReceipt)
                    .collect(Collectors.toList());

        }
        public List<Receipt> findReceiptEntitiesById(Long id) {
            return receiptRepository.findReceiptEntitiesById(id).stream()
                    .map(ReceiptMapper.INSTANCE::convertEntityReceiptToReceipt)
                    .collect(Collectors.toList());

        }
        @Transactional
        public void saveDelivery(Receipt receipt, String referenceOrder) {
            UserEntity currentUser = authenticationFacade.currentUser();
            OrderEntity order = orderService.findByReference(referenceOrder);

            ReceiptEntity receiptEntity = ReceiptMapper.INSTANCE.convertReceiptToReceiptEntity(receipt);
            receiptEntity.setClientEntity(order.getClientEntity())
                         .setUserCreated(currentUser);


            ReceiptEntity savedReceipt = receiptRepository.save(receiptEntity);

            // save receipt detail
            receipt.getReceiptDetailsEntities().forEach(receiptDetail -> {
                ReceiptDetailsEntity receiptDetailsEntity = new ReceiptDetailsEntity();
                StockEntity stockEntity = receiptDetail.getStock();

                receiptDetailsEntity.setReceipt(savedReceipt)
                        .setUnit(null != stockEntity.getUnit() ? stockEntity.getUnit().getUnit() : null)
                        .setStock(stockEntity)
                        .setL(receiptDetail.getL())
                        .setH(receiptDetail.getH())
                        .setW(receiptDetail.getW())
                        .setPallets(receiptDetail.getPallets())
                        .setComments(receiptDetail.getComments())
                        .setDescription(receiptDetail.getDescription())
                        .setLineCBM(receiptDetail.getLineCBM())
                        .setUnitCBM(receiptDetail.getUnitCBM())
                        .setOtherDescription(receiptDetail.getOtherDescription())
                        .setType(receiptDetail.getType())
                        .setSuppliedBy(receiptDetail.getSuppliedBy())
                        .setUnitsPerSize(receiptDetail.getUnitsPerSize())
                        .setUserCreated(currentUser);

                receiptDetailsRepository.save(receiptDetailsEntity);

            });

            // update stock quantity
            Map<StockEntity, Integer> receiptOrders = receipt.getReceiptDetailsEntities().stream()
                    .collect(Collectors.groupingBy(ReceiptDetailsEntity::getStock, Collectors.summingInt(ReceiptDetailsEntity::getPallets)));
            receiptOrders.forEach((stockEntity, receiptQt) -> {
                stockEntity.setBalanceStockQty(stockEntity.getBalanceStockQty() + receiptQt);
                stockRepository.save(stockEntity);
            });

            // update order status
            orderService.updateOrderStatus(order.getReference(), OrderStatus.RECEIPT);

        }
}
