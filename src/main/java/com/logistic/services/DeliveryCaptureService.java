package com.logistic.services;

import com.logistic.entities.DeliveryCaptureEntity;
import com.logistic.entities.model.DeliveryCapture;
import com.logistic.exception.MessageException;
import com.logistic.exception.ResourceNotFoundException;
import com.logistic.mapper.DeliveryCaptureMapper;
import com.logistic.repositories.DeliveryCaptureRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.math.BigInteger;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;



@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class DeliveryCaptureService {
    private final DeliveryCaptureRepository deliveryCaptureRepository;

    public List<DeliveryCapture> findAllDeliveriesCaptureByDeliveryEntityId(Long DeliveryId) {
        return   deliveryCaptureRepository.findAllByDeliveryId(DeliveryId).stream()
                .map(DeliveryCaptureMapper.INSTANCE::convertEntityDeliveryCaptureToDeliveryCapture)
                .collect(Collectors.toList());
    }
    public List<DeliveryCapture> findAllDeliveriesCaptureByStockEntityId(BigInteger StockId) {
        return   deliveryCaptureRepository.findAllByStockId(StockId).stream()
                .map(DeliveryCaptureMapper.INSTANCE::convertEntityDeliveryCaptureToDeliveryCapture)
                .collect(Collectors.toList());
    }
    public List<DeliveryCapture> findAllDeliveriesByFilter(String recherche) {
        return deliveryCaptureRepository.findByCriteria(recherche).stream()
                .map(DeliveryCaptureMapper.INSTANCE::convertEntityDeliveryCaptureToDeliveryCapture)
                .collect(Collectors.toList());

    }
    public List<DeliveryCapture> findAll() {
        return deliveryCaptureRepository.findAll().stream()
                .map(DeliveryCaptureMapper.INSTANCE::convertEntityDeliveryCaptureToDeliveryCapture)
                .collect(Collectors.toList());

    }
    public List<DeliveryCapture> findDeliveryCaptureEntitiesByQtyDelivered(Long qtyDelivered) {
        return deliveryCaptureRepository.findDeliveryCaptureEntitiesByQtyDelivered(qtyDelivered).stream()
                .map(DeliveryCaptureMapper.INSTANCE::convertEntityDeliveryCaptureToDeliveryCapture)
                .collect(Collectors.toList());

    }
    @Transactional
    public DeliveryCapture saveDeliveryCapture(DeliveryCapture deliveryCapture) {
        Optional<DeliveryCaptureEntity> optionalDeliveryCapture = deliveryCaptureRepository.findDeliveryCaptureEntitiesByQtyDelivered(deliveryCapture.getQtyDelivered());
        DeliveryCaptureEntity deliveryCaptureEntity ;

        if (optionalDeliveryCapture.isPresent()) {
            deliveryCaptureEntity = optionalDeliveryCapture.get();
            deliveryCaptureEntity.setQtyDelivered(deliveryCapture.getQtyDelivered());
            deliveryCaptureEntity.setUnit(deliveryCapture.getUnit());
            deliveryCaptureEntity.setComments(deliveryCapture.getComments());
            deliveryCaptureEntity.setDelivery(deliveryCaptureEntity.getDelivery());
            //deliveryCaptureEntity.setStock(deliveryCapture.getStock());
        }else {
            return DeliveryCaptureMapper.INSTANCE.convertEntityDeliveryCaptureToDeliveryCapture(optionalDeliveryCapture
                    .orElseThrow(() -> new ResourceNotFoundException(MessageException.RESOURCE_FOUND)));

        }

        return saveDeliveryCapture(deliveryCapture);
    }
}
