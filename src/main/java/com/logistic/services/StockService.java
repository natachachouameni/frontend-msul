package com.logistic.services;

import com.logistic.entities.StockEntity;
import com.logistic.entities.UoMEntity;
import com.logistic.entities.model.Stock;
import com.logistic.exception.MessageException;
import com.logistic.exception.ResourceNotFoundException;
import com.logistic.mapper.StockMapper;
import com.logistic.repositories.StockRepository;
import com.logistic.repositories.UoMRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class StockService {
    private final StockRepository stockRepository;
    private final UoMRepository uoMRepository;

    public List<Stock> findAllStockByReferenceClient(String referenceClient) {
        return   stockRepository.findAllByClientEntityReference(referenceClient).stream()
                .map(StockMapper.INSTANCE::convertEntityStockToStock)
                .collect(Collectors.toList());
    }
    public List<Stock> findAllStockByFilter(String recherche) {
        return stockRepository.findByCriteria(recherche).stream()
                .map(StockMapper.INSTANCE::convertEntityStockToStock)
                .collect(Collectors.toList());

    }
    public List<Stock> findAll() {
        return stockRepository.findAll().stream()
                .map(StockMapper.INSTANCE::convertEntityStockToStock)
                .collect(Collectors.toList());

    }
    public List<Stock> findReceiptEntitiesById(Long id) {
        return stockRepository.findStockEntitiesById(id).stream()
                .map(StockMapper.INSTANCE::convertEntityStockToStock)
                .collect(Collectors.toList());

    }
    @Transactional
    public Stock saveStock(Stock stock) {
        Optional<StockEntity> optionalStock = stockRepository.findStockEntitiesById(stock.getId());
       StockEntity stockEntity ;

        UoMEntity uoMEntity = uoMRepository.findByUnit(stock.getUnit())
                .orElseThrow(() -> new ResourceNotFoundException(String.format("L'unité %s n'existe pas", stock.getUnit())));

        if (optionalStock.isPresent()) {
            stockEntity = optionalStock.get();
            stockEntity.setReference(stock.getReference());
            stockEntity.setDateInStart(stock.getDateInStart());
            stockEntity.setType(stock.getType());
            stockEntity.setDescription(stockEntity.getDescription());
            stockEntity.setOtherDescription(stock.getOtherDescription());
            stockEntity.setUnit(uoMEntity);
            stockEntity.setL(stock.getL());
            stockEntity.setW(stock.getW());
            stockEntity.setClientEntity(stockEntity.getClientEntity());
            stockEntity.setH(stock.getH());
            stockEntity.setContainerNoType(stock.getContainerNoType());
            stockEntity.setBalanceStockQty(stock.getBalanceStockQty());
        }else {
            return StockMapper.INSTANCE.convertEntityStockToStock(optionalStock
                    .orElseThrow(() -> new ResourceNotFoundException(MessageException.RESOURCE_FOUND)));

        }

        return saveStock(stock);
    }
}
