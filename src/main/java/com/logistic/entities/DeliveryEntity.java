package com.logistic.entities;

import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Entity
@Getter
@ToString
@Setter
@RequiredArgsConstructor
@Table(name = "Delivery")
public class DeliveryEntity extends AbstractEntity {
    @Id
    @Column(name = "DeliveryNo")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="reference")
    @Size(max = 20, min = 16)
    private String reference;

    @Column(name = "DateOut")
    private LocalDateTime dateOut;

    @Column(name = "VehicleNo")
    private String vehicleNo;

    @Column(name = "Driver")
    private String driver;

    @Column(name = "TimeIn")
    private LocalTime timeIn;

    @Column(name = "TimeOut")
    private LocalTime timeOut;
    @Column(name = "ClientRef")
    private String clientRef;

    @Column(name = "Remarks")
    private String remarks;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "order_id")
    private OrderEntity order;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "client")
    private ClientEntity clientEntity;

}
