package com.logistic.entities;

import com.logistic.enums.OrderStatus;
import com.logistic.enums.OrderType;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Getter
@ToString
@Setter
@Accessors(chain = true)
@RequiredArgsConstructor
@Table(name = "orders")
public class OrderEntity extends AbstractEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name="reference", unique = true)
    @Size(max = 50)
    private String reference;
    @Lob
    private String description;
    @Enumerated
    private OrderType type;
    @Enumerated
    private OrderStatus status;

    private LocalDateTime deliveryDate;
    private LocalDateTime receiptDate;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "client")
    private ClientEntity clientEntity;
    @OneToMany(mappedBy = "order", fetch = FetchType.LAZY)
    private List<OrderItemEntity> itemEntities;

}
