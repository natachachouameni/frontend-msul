package com.logistic.entities;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.persistence.*;

@Getter
@Setter
@ToString
@Entity
@Accessors(chain = true)
@Table(name = "Delivery_Details")
public class DeliveryDetailsEntity extends AbstractEntity{
    @Id
    @Column(name = "DeliveryDetailNo")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "QtyDelivered")
    private int qtyDelivered;

    @Column(name = "Unit")
    private String unit;

    @Column(name = "Comments")
    private String comments;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false)
    @PrimaryKeyJoinColumn(name = "DeliveryNo")
    private DeliveryEntity deliveryEntity;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name="ItemCode")
    @PrimaryKeyJoinColumn(name="ItemCode")
    private StockEntity stockEntity;
}
