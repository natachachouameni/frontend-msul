package com.logistic.entities;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Getter
@ToString
@Setter
@Accessors(chain = true)
@RequiredArgsConstructor
@Table(name = "orderItem")
public class OrderItemEntity extends AbstractEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String description;
    private Integer quantity;

    @Column(name = "L")
    private int l;

    @Column(name = "W")
    private int w;

    @Column(name = "H")
    private int h;
    @Column(name = "ContainerNoType")
    private String containerNoType;
    @Column(name = "UnitCBM")
    private String unitCBM;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "order_id")
    private OrderEntity order;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "stock_id")
    private StockEntity stock;

}
