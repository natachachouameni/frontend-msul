package com.logistic.entities;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Setter
@RequiredArgsConstructor
@ToString
@Getter
@Table(name = "Delivery_Capture")

public class DeliveryCaptureEntity extends AbstractEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id ;

    @Column(name = "QtyDelivered")
    private Long qtyDelivered;

    @Column(name = "Unit")
    private String unit;

    @Column(name = "Comments")
    private String comments;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "DeliveryNo")
    private DeliveryEntity delivery;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "ItemCode")
    private StockEntity stock;

}
