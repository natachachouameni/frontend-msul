package com.logistic.entities;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Getter
@Setter
@ToString
@Entity
@RequiredArgsConstructor
@Table(name = "Receipt_Capture")

public class ReceiptCaptureEntity extends AbstractEntity {
    @Id
    @Column(name = "QtyReceived")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long qtyReceived;

    @Column(name = "Unit")
    private String unit;

    @Column(name = "Comments")
    private String comments;

    @Column(name = "Pallets")
    private int pallets;

    @Column(name = "SuppliedBy")
    private String suppliedBy;

    @Column(name = "L")
    private int l;

    @Column(name = "W")
    private int w;

    @Column(name = "H")
    private int h;

    @Column(name = "UnitsPerSize")
    private int unitsPerSize;
    @Column(name = "TRIAL557")
    private String trial557;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name="GRNNo")
    private ReceiptEntity receiptEntity;
}
