package com.logistic.entities.model;


import com.logistic.entities.StockEntity;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Builder
public class StockOrder {
    private StockEntity stock;
    private int quantity;
}
