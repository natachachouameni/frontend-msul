package com.logistic.entities.model;

import com.logistic.entities.UserEntity;
import com.logistic.validator.AdresseEmail;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import lombok.*;

import java.time.LocalDateTime;
import java.util.UUID;

@Builder
@Getter
@Setter
public class Client {
    private String reference =  String.format("MST%s",UUID.randomUUID().toString());

    @NotBlank(message = "Le nom de la compagnie est obligatoire")
    private String companyName;

    @NotBlank(message = "Le nom est obligatoire")
    private  String firstname;
    private String lastname;
    private String mainContact;
    private String contactDetail;
    private String phoneNumber;
    private String address;
    @AdresseEmail
    private String login;
    @Size(min = 8, message = "Le mot de passe est obligatoire")
    private String password;
}
