package com.logistic.entities.model;

import com.logistic.entities.ReceiptEntity;
import com.logistic.entities.StockEntity;
import lombok.*;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ReceiptCapture {

        private Long qtyReceived;


        private String unit;


        private String comments;


        private int pallets;


        private String suppliedBy;


        private int l;


        private int w;


        private int h;


        private int unitsPerSize;

        private String trial557;

        private Stock stock;
        private Receipt receipt;
}
