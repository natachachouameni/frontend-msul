package com.logistic.entities.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
public class ErrorInfo {
    Integer code;
    String message;
}
