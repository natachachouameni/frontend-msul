package com.logistic.entities.model;

import lombok.*;

import java.time.LocalDateTime;
import java.time.LocalTime;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Delivery {
    private  Long id;
    private String reference;

    private LocalDateTime dateOut;

    private String vehicleNo;

    private String driver;

    private LocalTime timeIn;

    private LocalTime timeOut;

    private String clientRef;

    private String remarks;

    private String trial537;

    private Client client;
}
