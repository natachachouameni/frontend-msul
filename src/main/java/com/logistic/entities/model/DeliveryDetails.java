package com.logistic.entities.model;


import com.logistic.entities.StockEntity;
import lombok.*;



@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DeliveryDetails {

    private Long id;


    private int qtyDelivered;


    private String unit;


    private String comments;

    private Delivery delivery;

    private Stock stock;

}
