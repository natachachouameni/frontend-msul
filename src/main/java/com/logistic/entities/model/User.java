package com.logistic.entities.model;

import com.logistic.enums.AppRole;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.*;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class User {
    private Long id;

    @NotBlank(message = "Le login vide")
    @Email
    private String login;
    @NotBlank(message = "nom d'utilisateur vide")
    private String firstname;
    private String lastname;

    @NotBlank(message = "mot de passe vide")
    @Size(min = 8, message = "La longeur min est de 8 caractère")
    private String password;

    private String trial573;

    private LocalDateTime createdAt;

    private LocalDateTime updateAt;

    @NotNull(message = "Le role est vide")
    private AppRole role;
}
