package com.logistic.entities.model;

import com.logistic.enums.OrderStatus;
import com.logistic.enums.OrderType;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@ToString
@Builder
public class Order {
    private String reference;
    List<StockOrder> stockOrders;
    @NotBlank(message = "La description est vide")
    private String description;
    @NotNull(message = "Le type de la commande est vide")
    private OrderType type;
    private OrderStatus status;
    private LocalDateTime date;
}
