package com.logistic.entities.model;

import com.logistic.entities.DeliveryEntity;
import com.logistic.entities.StockEntity;
import lombok.*;


@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DeliveryCapture {

    private Long qtyDelivered;


    private String unit;


    private String comments;


    private String trial544;


    private Delivery delivery;


    private Stock stock;

}
