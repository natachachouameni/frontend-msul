package com.logistic.entities.model;

import lombok.*;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UoM {

    private Long UoM;

    private String unit;

    private String trial573;
}
