package com.logistic.entities.model;

import com.logistic.entities.ReceiptDetailsEntity;
import lombok.*;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;


@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Receipt {

    private Long id;
    private String reference;
    private Integer receiptNo;
    private LocalDateTime dateIn;
    private String vehicleNo;

    private String driver;

    private String containerNo;

    private String containerType;

    private String sealNo;
    private LocalTime timeIn;

    private LocalTime timeOut;

    private String clientRef;

    private String remarks;

    private Client client;

    List<ReceiptDetailsEntity> receiptDetailsEntities;
}
