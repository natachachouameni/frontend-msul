package com.logistic.entities.model;

import lombok.*;


import java.math.BigInteger;
import java.time.LocalDateTime;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Stock {

    private Long id;


    private String reference;


    private LocalDateTime dateInStart;


    private String type;


    private String description;

    private String otherDescription;

    private String unit;

    private int l;


    private int w;


    private int h;


    private String containerNoType;


    private Integer balanceStockQty;

    private String unitCBM;

    private Client client;
}
