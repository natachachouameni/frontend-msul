package com.logistic.entities;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Getter
@Setter
@Entity
@ToString
@Table(name = "UoM")
public class UoMEntity extends AbstractEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "UoM", unique = true)
    private String abr;
    @Column(name = "Unit", unique = true)
    private String unit;
}
