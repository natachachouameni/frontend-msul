package com.logistic.entities;

import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Getter
@Setter
@ToString
@Table(name = "Client")
public class ClientEntity extends AbstractEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name="Client")
    @Size(max = 20, min = 16)
    private String reference;

    @Column(name = "ClientName")
    private String companyName;

    @Column(name="lastName")
    private String lastname;

    @Column(name="firstName")
    private String firstname;

    @Column(name = "phoneNumber")
    private String phoneNumber;
    @Column(name = "MainContact")
    private String mainContact;
    @Column(name = "ContactDetail")
    private String address;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_client_id", referencedColumnName = "id")
    private UserEntity user;

}
