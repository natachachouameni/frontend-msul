package com.logistic.entities;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.*;

@Getter
@Setter
@Entity
@RequiredArgsConstructor
@ToString
@Table(name = "Users")
public class UserEntity extends AbstractEntity {
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "login", unique = true)
    private String login;

    @Column(name = "Password")
    private String password;

    @Column(name = "TRIAL573")
    private String trial573;

    private String firstname;
    private String lastname;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "user", fetch = FetchType.LAZY)
    private ClientEntity client;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "users_roles",
            joinColumns = {
            @JoinColumn(name = "user_id", nullable = false)
            },
            inverseJoinColumns = {
            @JoinColumn(name = "role_id", nullable = false)
            }
    )
    private List<RoleEntity> roles = new ArrayList<>();

    public boolean hasRole(RoleEntity role) {
        if(Objects.isNull(role)) {
            return false;
        }

        return roles.contains(role);
    }

    public void addRole(RoleEntity role){
        roles = new ArrayList<>();
        roles.add(role);
    }

}
