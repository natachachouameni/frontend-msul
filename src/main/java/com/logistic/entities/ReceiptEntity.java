package com.logistic.entities;

import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Entity
@Getter
@Setter
@ToString
@Accessors(chain = true)
@Table(name = "Receipt")
public class ReceiptEntity extends AbstractEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="GRNNo")
    @Size(max = 20, min = 16)
    private String reference;

    @Column(name = "ReceiptNo")
    private Integer receiptNo;

    @Column(name = "DateIn")
    private LocalDateTime dateIn;

    @Column(name = "VehicleNo")
    private String vehicleNo;

    @Column(name = "Driver")
    private String driver;

    @Column(name = "ContainerNo")
    private String containerNo;

    @Column(name = "ContainerType")
    private String containerType;

    @Column(name = "SealNo")
    private String sealNo;
    @Column(name = "TimeIn")
    private LocalTime timeIn;

    @Column(name = "TimeOut")
    private LocalTime timeOut;
    @Column(name = "ClientRef")
    private String clientRef;

    @Column(name = "Remarks")
    private String remarks;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "order_id")
    private OrderEntity order;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "client")
    private ClientEntity clientEntity;

}
