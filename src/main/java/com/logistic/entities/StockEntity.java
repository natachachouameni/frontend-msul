package com.logistic.entities;

import javax.validation.constraints.Size;

import com.logistic.entities.model.UoM;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigInteger;
import java.time.LocalDateTime;

@Getter
@Setter
@ToString
@Entity
@Table(name = "Stock")
public class StockEntity extends AbstractEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="ItemCode", unique = true)
    private String reference;

    @Column(name = "DateInStart")
    private LocalDateTime dateInStart;

    @Column(name = "Type")
    private String type;

    @Column(name = "Description")
    private String description;

    @Column(name = "OtherDescription")
    private String otherDescription;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.DETACH)
    @JoinColumn(name = "uom_id", nullable = true)
    private UoMEntity unit;
    @Column(name = "L")
    private int l;

    @Column(name = "W")
    private int w;

    @Column(name = "H")
    private int h;

    @Column(name = "ContainerNoType")
    private String containerNoType;

    @Column(name = "BalanceStockQty")
    private Integer balanceStockQty;

    @Column(name = "UnitCBM")
    private String unitCBM;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name="Client")
    private ClientEntity clientEntity;
}
