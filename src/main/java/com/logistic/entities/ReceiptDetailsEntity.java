package com.logistic.entities;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.persistence.*;

@Getter
@Setter
@Entity
@ToString
@Accessors(chain = true)
@Table(name = "Receipt_Details")
public class ReceiptDetailsEntity extends AbstractEntity {
    @Id
    @Column(name = "ReceiptDetailNo")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long receiptDetailNo;
    @Column(name = "Type")
    private String type;

    @Column(name = "Description")
    private String description;

    @Column(name = "OtherDescription")
    private String otherDescription;

    @Column(name = "Unit")
    private String unit;

    @Column(name = "Comments")
    private String comments;

    @Column(name = "Pallets")
    private int pallets;

    @Column(name = "SuppliedBy")
    private String suppliedBy;

    @Column(name = "L")
    private int l;

    @Column(name = "W")
    private int w;

    @Column(name = "H")
    private int h;

    @Column(name = "UnitsPerSize")
    private int unitsPerSize;

    @Column(name = "UnitCBM")
    private String unitCBM;
    @Column(name = "LineCBM")
    private String lineCBM;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name="GRNNo")
    private ReceiptEntity receipt;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name="ItemCode")
    private StockEntity stock;

}
