package com.logistic.utils;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NavigationAttribute implements Comparable<NavigationAttribute>{
    private int position;
    private String page1;
    private String page2;
    private String page3;
    private String lien;

    @Override
    public int compareTo(NavigationAttribute o) {
        return this.position - o.getPosition();
    }
}
