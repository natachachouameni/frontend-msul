package com.logistic.security;

import com.logistic.entities.UserEntity;
import org.springframework.security.core.Authentication;

public interface IAuthenticationFacade {
    Authentication getAuthentication();

    UserEntity currentUser();
}
