package com.logistic.security;

import com.logistic.entities.UserEntity;
import com.logistic.exception.UserException;
import com.logistic.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class AuthenticationFacade  implements IAuthenticationFacade{
    @Autowired
    private UserRepository userRepository;
    @Override
    public Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }
    @Override
    public UserEntity currentUser() {
        return userRepository.findByLogin(getAuthentication().getName())
                .orElseThrow(() -> new UserException("Aucun utilisateur connecté"));
    }
}
