package com.logistic.controller;

import com.logistic.entities.UserEntity;
import com.logistic.entities.model.User;
import com.logistic.enums.AppRole;
import com.logistic.exception.UserException;
import com.logistic.services.UserService;
import javax.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@AllArgsConstructor
@RequestMapping("/users")
@Slf4j
public class UserController {
    private final UserService userService;

    @GetMapping("")
    @PreAuthorize("hasRole('ADMIN')")
    public String index(Model model) {
        List<UserEntity> users = userService.findAllUsers();
        model.addAttribute("users", users);
        return "app/user/index";
    }

    @GetMapping("/edit")
    @PreAuthorize("hasRole('ADMIN')")
    public String edit(Model model) {
        buildSaveUserModel(new User(), model);
        return "app/user/edit";
    }

    @PostMapping("/store")
    @PreAuthorize("hasRole('ADMIN')")
    public String store(@Valid @ModelAttribute("user") User user, BindingResult result, Model model) {
        log.info("store user");
        log.info(user.toString());
        if (result.hasErrors()) {
            buildSaveUserModel(user, model);
            return "app/user/edit";
        }

        try {
            userService.saveUser(user);
        } catch (UserException.UserAlreadyExistException e) {
            result.rejectValue("email", null,
                    "Un utilisateur existe déjà avec cet email");
            buildSaveUserModel(user, model);
            return "app/user/edit";
        }

        return "redirect:/users";
    }

    private void buildSaveUserModel(User user, Model model) {
        List<AppRole> roles = userService.getAllRole();
        model.addAttribute("roles", roles);
        model.addAttribute("user", user);
    }
}
