package com.logistic.controller;

import com.logistic.entities.model.Client;
import com.logistic.services.ClientService;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequiredArgsConstructor
@RequestMapping("/clients")
@Slf4j
public class ClientController {
    private final ClientService clientService;
    @GetMapping
    @PreAuthorize("hasRole('ADMIN')")
    public String listClient(Model model) {
        List<Client> clients = clientService.findAllClient();
        model.addAttribute("clients", clients);
        return "app/client/index";
    }

    @GetMapping("/edit")
    @PreAuthorize("hasRole('ADMIN')")
    public String editClient(Model model) {
        model.addAttribute("client", Client.builder().build());
        return "app/client/edit";
    }

    @PostMapping("/store")
    @PreAuthorize("hasRole('ADMIN')")
    public String saveClient(@Valid @ModelAttribute("client") Client client, BindingResult result, Model model) {
        if (result.hasErrors()) {
            log.error("error");
            System.out.println(result);
            model.addAttribute("client", client);
            return "app/client/edit";
        }
        clientService.saveClient(client);
        return "redirect:/clients";
    }
}
