package com.logistic.repositories;

import com.logistic.entities.DeliveryDetailsEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

@Repository
public interface DeliveryDetailsRepository extends JpaRepository<DeliveryDetailsEntity, Long> {
    List<DeliveryDetailsEntity> findAllByDeliveryEntityId(Long id);
    List<DeliveryDetailsEntity> findAllByStockEntityId(BigInteger id);
    List<DeliveryDetailsEntity> findAll();
    Optional<DeliveryDetailsEntity> findDeliveryDetailsEntitiesById(Long id);
    @Query(
            "SELECT DeliveryDetails FROM DeliveryDetailsEntity  DeliveryDetails WHERE DeliveryDetails.id = :recherche OR  DeliveryDetails.qtyDelivered = :recherche OR   DeliveryDetails.unit = :recherche Or DeliveryDetails.stockEntity = :recherche Or DeliveryDetails.deliveryEntity = :recherche  "
    )
    List<DeliveryDetailsEntity> findByCriteria(String recherche);

}
