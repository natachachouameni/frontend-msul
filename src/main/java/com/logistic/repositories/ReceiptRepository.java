package com.logistic.repositories;

import com.logistic.entities.DeliveryEntity;
import com.logistic.entities.ReceiptEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ReceiptRepository extends JpaRepository<ReceiptEntity, Integer> {

    /**
     * Find all Delivery by client reference
     * @param reference reference client
     * @return list of delivery
     */
    List<ReceiptEntity> findAllByClientEntityReference(String reference);


    List<ReceiptEntity> findAll();
    Optional<ReceiptEntity> findReceiptEntitiesById(Long id);
    @Query(
            "SELECT Receipt FROM ReceiptEntity  Receipt WHERE Receipt.id = :recherche OR  Receipt.driver = :recherche OR   Receipt.dateIn = :recherche Or Receipt.vehicleNo = :recherche OR Receipt.containerNo = :recherche OR   Receipt.containerType = :recherche Or Receipt.sealNo = :recherche Or Receipt.receiptNo = :recherche Or Receipt.clientEntity = :recherche  "
    )
    List<ReceiptEntity> findByCriteria(String recherche);

}
