package com.logistic.repositories;

import com.logistic.entities.ClientEntity;
import com.logistic.entities.DeliveryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DeliveryRepository extends JpaRepository<DeliveryEntity, Long> {

    /**
     * Find all Delivery by client reference
     * @param reference reference client
     * @return list of delivery
     */
    List<DeliveryEntity> findAllByClientEntityReference(String reference);


    List<DeliveryEntity> findAll();
   //Optional <DeliveryEntity> findById(Long id);
    @Query(
            "SELECT Delivery FROM DeliveryEntity Delivery WHERE Delivery.id = :recherche OR  Delivery.driver = :recherche OR   Delivery.dateOut = :recherche Or Delivery.vehicleNo = :recherche Or Delivery.clientEntity = :recherche  "
    )
    List<DeliveryEntity> findByCriteria(String recherche);



}
