package com.logistic.repositories;

import com.logistic.entities.ClientEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ClientRepository extends JpaRepository<ClientEntity, Long> {
    Optional<ClientEntity> findByReference(String reference);

    @Query(
            "SELECT client FROM ClientEntity client WHERE client.reference = :recherche OR  client.companyName = :recherche"
    )
    List<ClientEntity> findByCriteria(String recherche);
}
