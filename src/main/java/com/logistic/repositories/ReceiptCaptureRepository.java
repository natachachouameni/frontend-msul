package com.logistic.repositories;

import com.logistic.entities.ReceiptCaptureEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface  ReceiptCaptureRepository extends JpaRepository<ReceiptCaptureEntity,Long> {
}
