package com.logistic.repositories;

import com.logistic.entities.OrderEntity;
import com.logistic.enums.OrderStatus;
import com.logistic.enums.OrderType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface OrderRepository extends JpaRepository<OrderEntity, Long> {
    Optional<OrderEntity> findByReference(String reference);

    @Query("SELECT order FROM OrderEntity order "
            + "WHERE (:status IS NOT NULL OR order.status =:status) "
            + "AND (:type IS NOT NULL OR order.type =:type)")
    List<OrderEntity> findAllByStatusAndType(OrderStatus status, OrderType type);

    @Query("SELECT O FROM OrderEntity O where O.reference = :search "
//            + "JOIN  OrderItemEntity OI ON OI.order = order "
//            + "WHERE (:seach IS NOT NULL OR order.reference LIKE %:search%) "
//            + "OR (:seach IS NOT NULL OR order.description LIKE %:search%)"
//            + "OR (:seach IS NOT NULL OR OI.stock.reference LIKE %:search%)"
//            + "OR (:seach IS NOT NULL OR OI.stock.description LIKE %:search%)"
    )
    List<OrderEntity> findAllByFilter(String search);

}
