package com.logistic.repositories;

import com.logistic.entities.UoMEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UoMRepository extends JpaRepository<UoMEntity, Long> {
    Optional<UoMEntity> findByUnit(String unit);
    Optional<UoMEntity> findByAbr(String abr);
}
