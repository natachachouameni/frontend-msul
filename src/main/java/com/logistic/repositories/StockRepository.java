package com.logistic.repositories;

import com.logistic.entities.ReceiptEntity;
import com.logistic.entities.StockEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public interface StockRepository extends JpaRepository<StockEntity, BigInteger> {
    /**
     * Find all Delivery by client reference
     * @param reference reference client
     * @return list of delivery
     */
    Optional<StockEntity> findAllByClientEntityReference(String reference);

    List<StockEntity> findAllByIdIn(Set<Long> ids);

    Optional<StockEntity> findStockEntitiesById(Long id);
    @Query(
            "SELECT Stock FROM StockEntity  Stock WHERE Stock.id = :recherche OR  Stock.dateInStart = :recherche OR   Stock.type = :recherche Or Stock.description = :recherche OR Stock.otherDescription = :recherche OR   Stock.l = :recherche Or Stock.w = :recherche Or Stock.h = :recherche  Or Stock.containerNoType = :recherche Or Stock.balanceStockQty = :recherche  Or Stock.unit = :recherche Or Stock.clientEntity = :recherche      "
    )
    List<StockEntity> findByCriteria(String recherche);

}
