package com.logistic.repositories;

import com.logistic.entities.ReceiptDetailsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReceiptDetailsRepository extends JpaRepository<ReceiptDetailsEntity, Long> {

}
