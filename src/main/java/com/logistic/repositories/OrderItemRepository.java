package com.logistic.repositories;

import com.logistic.entities.OrderEntity;
import com.logistic.entities.OrderItemEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface OrderItemRepository extends JpaRepository<OrderItemEntity, Long> {
    List<OrderItemEntity> findAllByOrderReference(String reference);

    @Modifying
    @Query("DELETE FROM OrderItemEntity orderIten WHERE orderIten.stock.reference =:reference")
    void removeAllOrderIdemByOrderReference(String reference);
}
