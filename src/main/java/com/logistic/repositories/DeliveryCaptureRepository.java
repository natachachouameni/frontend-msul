package com.logistic.repositories;

import com.logistic.entities.DeliveryCaptureEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

@Repository
public interface DeliveryCaptureRepository extends JpaRepository<DeliveryCaptureEntity, Long> {
    List<DeliveryCaptureEntity> findAllByDeliveryId(Long id);

    List<DeliveryCaptureEntity> findAllByStockId(BigInteger id);

    List<DeliveryCaptureEntity> findAll();

    Optional<DeliveryCaptureEntity> findDeliveryCaptureEntitiesByQtyDelivered(Long qtyDelivered);

    @Query(
            "SELECT delivery FROM DeliveryCaptureEntity  delivery WHERE "
                    +"delivery.qtyDelivered = :recherche OR   "
                    +"delivery.unit = :recherche "
                    +"Or delivery.delivery = :recherche Or delivery.stock = :recherche   "
    )
    List<DeliveryCaptureEntity> findByCriteria(String recherche);
}
