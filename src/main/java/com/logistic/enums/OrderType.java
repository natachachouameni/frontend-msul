package com.logistic.enums;

public enum OrderType {
    DELIVERY,
    RECEIPT
}
