package com.logistic.enums;

import lombok.Getter;

@Getter
public enum AppRole {
    ROLE_SUPER_AMIN(0, "Super admin"),
    ROLE_ADMIN(5, "admin"),
    ROLE_MANAGER(6, "Manager"),
    ROLE_USER(20, "Utilisateur");

    private final int position;
    private final String designation;

    AppRole(int position, String designation) {
        this.position = position;
        this.designation = designation;
    }
}
