package com.logistic.enums;

public enum OrderStatus {
    EN_COURS,
    ANNULER,
    VALIDER,
    RECEIPT,
    DELIVERED,
    EN_ATTENTE;
}
